package com.ead.course.utils;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import org.springframework.util.ReflectionUtils;


@Component
public class MapperUtils<T> {	
	
	Logger logger =  LogManager.getLogger(this.getClass());
	
	public MapperUtils() {}
	
	public MapperUtils(Class<T> type) {
		this.type = type;
	}	

	private ModelMapper mapper = new ModelMapper();
	
	private Class<T> type;	
	
	public void merge(Map<String, Object> fields, T entityTarget) {        
        Class<T> entityClass = (Class<T>) entityTarget.getClass();
        try {
            T entityOrigin = mapper.map(fields, entityClass);
            if (fields != null && entityTarget != null) {
                fields.forEach((key, value) -> {
                    Field field = ReflectionUtils.findField(entityClass, key);
                    if (field == null) throw new RuntimeException("Field not found in entity");
                    field.setAccessible(true);
                    Object newValue = ReflectionUtils.getField(field, entityOrigin);
                    ReflectionUtils.setField(field, entityTarget, newValue);
                });
            }
        } catch (Exception e) {
        	logger.error(e.getMessage());
        }
    }
	
	public T toModel(Object object) {			
		return mapper.map(object,type);				
	}
	
	public List<T> toCollection(Collection<?> modelList){		
		return modelList.stream()
			.map((domainModel) -> toModel(domainModel))
			.collect(Collectors.toList());
	}

}
