package com.ead.course.api.dto;

import java.time.OffsetDateTime;
import java.util.UUID;

import com.ead.course.enums.CourseLevelEnum;
import com.ead.course.enums.CourseStatusEnum;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter @Setter
public class CourseResponseDTO {
	
	private UUID courseId;	
	private String name;
	private String description;	
	private String imageUrl;	
	private UUID userInstructor;		
	private CourseStatusEnum courseStatus;	
	private CourseLevelEnum courseLevel;		
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss'Z'")
	private OffsetDateTime creationDate;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss'Z'")
	private OffsetDateTime lastUpdateDate;

}
