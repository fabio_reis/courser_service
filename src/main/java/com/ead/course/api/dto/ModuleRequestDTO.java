package com.ead.course.api.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter @Setter
public class ModuleRequestDTO {		
	
	private String title;		
	private String description;		

}
