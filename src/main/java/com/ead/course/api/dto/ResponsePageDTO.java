package com.ead.course.api.dto;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonCreator.Mode;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;


public class ResponsePageDTO<T> extends PageImpl<T> {
	
	private static final long serialVersionUID = 1L;	
	
	public ResponsePageDTO(List<T> content, Pageable pageable, long total) {
		super(content, pageable, total);		
	}
	
	public ResponsePageDTO(List<T> content) {
		super(content);		
	}
	
	public ResponsePageDTO() {
		super(new ArrayList<T>());		
	}
	
	@JsonCreator(mode = Mode.PROPERTIES)	
	public ResponsePageDTO(
			@JsonProperty("content") List<T> content,
			@JsonProperty("number") int number,
			@JsonProperty("size") int size,
            @JsonProperty("totalElements") Long totalElements,
            @JsonProperty("pageable") JsonNode pageable,
            @JsonProperty("last") boolean last,
            @JsonProperty("totalPages") int totalPages,
            @JsonProperty("sort") JsonNode sort,
            @JsonProperty("first") boolean first,
            @JsonProperty("numberOfElements") int numberOfElements){
		
		super(content, PageRequest.of(number, size), totalElements);
		
	} 	

}
