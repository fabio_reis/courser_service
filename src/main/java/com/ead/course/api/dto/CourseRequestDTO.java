package com.ead.course.api.dto;

import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter @Setter
public class CourseRequestDTO {	
	
	private UUID courseId;
	private String name;
	private String description;	
	private String imageUrl;	
	private UUID userInstructor;		
	private String courseStatus;	
	private String courseLevel;			

}
