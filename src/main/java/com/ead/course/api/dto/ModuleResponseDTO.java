package com.ead.course.api.dto;

import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter @Setter
public class ModuleResponseDTO {
	
	private UUID moduleId;	
	private String title;	
	private String description;			

}
