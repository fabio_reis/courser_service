package com.ead.course.api.mapper;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ead.course.api.dto.CourseRequestDTO;
import com.ead.course.api.dto.CourseResponseDTO;
import com.ead.course.enums.CourseLevelEnum;
import com.ead.course.enums.CourseStatusEnum;
import com.ead.course.model.CourseEntity;

@Component
public class CourseMapperDTO {
	
	@Autowired
	private ModelMapper mapper;
	
	public CourseResponseDTO toDTO(CourseEntity course) {		
		return mapper.map(course, CourseResponseDTO.class);	
	}
	
	public CourseEntity toEntity(CourseRequestDTO courseRequest) {		
		
		CourseStatusEnum.parse(courseRequest.getCourseStatus());
		CourseLevelEnum.parse(courseRequest.getCourseLevel());
		
		return mapper.map(courseRequest, CourseEntity.class);
	}
	
	public List<CourseResponseDTO> toCollectionModelDTO(List<CourseEntity> courses){
		return courses.stream()
				.map(course -> toDTO(course))
				.collect(Collectors.toList());
	}	

}
