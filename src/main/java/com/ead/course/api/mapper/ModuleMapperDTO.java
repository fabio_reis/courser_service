package com.ead.course.api.mapper;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ead.course.api.dto.ModuleRequestDTO;
import com.ead.course.api.dto.ModuleResponseDTO;
import com.ead.course.model.ModuleEntity;

@Component
public class ModuleMapperDTO {
	
	@Autowired
	private ModelMapper mapper;
	
	public ModuleResponseDTO toModelDTO(ModuleEntity module) {		
		return mapper.map(module, ModuleResponseDTO.class);	
	}
	
	public ModuleEntity toEntity(ModuleRequestDTO moduleRequest) {			
		return mapper.map(moduleRequest, ModuleEntity.class);
	}
	
	public List<ModuleResponseDTO> toCollectionModelDTO(List<ModuleEntity> modules){
		return modules.stream()
				.map(module -> toModelDTO(module))
				.collect(Collectors.toList());
	}	

}
