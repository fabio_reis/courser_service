package com.ead.course.api.mapper;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ead.course.api.dto.UserResponseDTO;
import com.ead.course.model.UserEntity;

@Component
public class UserMapperDTO {
	
	@Autowired
	private ModelMapper mapper;
	
	public UserResponseDTO toDTO(UserEntity user) {		
		return mapper.map(user, UserResponseDTO.class);	
	}
	
	/*
	 * public UserEntity toEntity(CourseRequestDTO courseRequest) {
	 * 
	 * CourseStatus.parse(courseRequest.getCourseStatus());
	 * CourseLevel.parse(courseRequest.getCourseLevel());
	 * 
	 * return mapper.map(courseRequest, CourseEntity.class); }
	 */
	
	public List<UserResponseDTO> toCollectionModelDTO(List<UserEntity> users){
		return users.stream()
				.map(user -> toDTO(user))
				.collect(Collectors.toList());
	}	

}
