package com.ead.course.api.mapper;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ead.course.api.dto.LessonRequestDTO;
import com.ead.course.api.dto.LessonResponseDTO;
import com.ead.course.model.LessonEntity;

@Component
public class LessonMapperDTO {
	
	@Autowired
	private ModelMapper mapper;
	
	public LessonResponseDTO toModelDTO(LessonEntity lesson) {		
		return mapper.map(lesson, LessonResponseDTO.class);	
	}
	
	public LessonEntity toEntity(LessonRequestDTO lessonRequest) {			
		return mapper.map(lessonRequest, LessonEntity.class);
	}
	
	public List<LessonResponseDTO> toCollectionModelDTO(List<LessonEntity> lessons){
		return lessons.stream()
				.map(lesson -> toModelDTO(lesson))
				.collect(Collectors.toList());
	}	

}
