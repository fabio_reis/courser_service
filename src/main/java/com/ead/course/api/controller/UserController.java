package com.ead.course.api.controller;

import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ead.course.api.mapper.UserMapperDTO;
import com.ead.course.service.UserService;

@RestController
@RequestMapping("v1/courses")
public class UserController {

	Logger logger = LogManager.getLogger(this.getClass());

	@Autowired
	private UserService userService;

	@Autowired
	private UserMapperDTO courseUserMapper;

	@GetMapping("{courseId}/users")
	public Page<?> findUsersByCourse(@PathVariable("courseId") UUID courseId,
			@PageableDefault(page = 0, size = 10) Pageable pageable) {

		logger.info("Try to get users by courseId: " + courseId);

		return new PageImpl<>(
				courseUserMapper.toCollectionModelDTO(userService.findAllUsersByCourseId(courseId, pageable)));

	}	
	
}
