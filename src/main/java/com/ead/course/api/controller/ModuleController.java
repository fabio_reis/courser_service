package com.ead.course.api.controller;

import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.ead.course.api.dto.ModuleRequestDTO;
import com.ead.course.api.mapper.ModuleMapperDTO;
import com.ead.course.model.CourseEntity;
import com.ead.course.model.ModuleEntity;
import com.ead.course.service.CourserService;
import com.ead.course.service.ModuleService;

@RestController
@RequestMapping("v1/courses/{courseId}/modules")
public class ModuleController {
	
	Logger logger =  LogManager.getLogger(this.getClass());	
	
	@Autowired
	private ModuleService moduleService;
	
	@Autowired
	private CourserService courseService;
	
	@Autowired
	private ModuleMapperDTO mapper;	 
	
	@GetMapping
	public ResponseEntity<?> findAll(
			@PathVariable UUID courseId,
			@PageableDefault(page = 0, size = 10) Pageable pageable){			
		
		 return ResponseEntity.ok(moduleService.findAllByCourseId(courseId, pageable));		
	
	}
	
	@GetMapping("{moduleId}")
	public ResponseEntity<?> findById(
			@PathVariable UUID courseId,
			@PathVariable UUID moduleId){		
		
		logger.info("Try Get Module object id: ", moduleId);
		
		 return ResponseEntity
				 .ok(mapper.toModelDTO(
						 moduleService.findByModuleIdAndCourseId(moduleId, courseId))
					);
	
	}	
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public void save(@PathVariable UUID courseId,  @RequestBody ModuleRequestDTO dto) {	
		
		logger.info("Try to post Module object: {} ", dto.toString());		
		
		CourseEntity course = courseService.findById(courseId);
		ModuleEntity module = mapper.toEntity(dto);	
		module.setCourse(course);
		
		moduleService.save(module);		
		
		logger.info("Module save success: {} ", dto.toString());
	}	
	
	@PutMapping("{moduleId}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void update(
			@PathVariable UUID moduleId,
			@PathVariable UUID courseId,
			@RequestBody ModuleRequestDTO dto) {
		
		logger.info("Try to put Module object: {} ", dto.toString());
		
		CourseEntity course = courseService.findById(courseId);
		
		ModuleEntity module = moduleService.findById(moduleId);
		BeanUtils.copyProperties(
				dto,
				module,
				"courseId"				
		);	
		
		module.setCourse(course);
		moduleService.save(module);		
		
		logger.info("Module update success: {} ", dto.toString());
		
	} 	
	
	@DeleteMapping("{moduleId}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void delete(@PathVariable UUID courseId, @PathVariable UUID moduleId) {
		
		logger.info("Try to delete ModuleId: ", moduleId, " courseId: " + courseId);
		
		courseService.findById(courseId);
		moduleService.delete(moduleId);
		
		logger.info("deleted ModuleId: ", moduleId + " courseId: " + courseId);
		
	}

}
