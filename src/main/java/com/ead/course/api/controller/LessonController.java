package com.ead.course.api.controller;

import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.ead.course.api.dto.LessonRequestDTO;
import com.ead.course.api.dto.ModuleRequestDTO;
import com.ead.course.api.mapper.LessonMapperDTO;
import com.ead.course.model.LessonEntity;
import com.ead.course.model.ModuleEntity;
import com.ead.course.service.LessonService;
import com.ead.course.service.ModuleService;

@RestController
@RequestMapping("v1/modules/{moduleId}/lessons")
public class LessonController {
	
	Logger logger =  LogManager.getLogger(this.getClass());
	
	@Autowired
	private LessonService lessonService;
	
	@Autowired
	private ModuleService moduleService;
	
	
	@Autowired
	private LessonMapperDTO mapper;		
	
	@GetMapping
	public ResponseEntity<?> findAll(
			@PathVariable UUID moduleId,
			@PageableDefault(page = 0, size = 10) Pageable pageable){
		
		moduleService.findById(moduleId);
		
		return ResponseEntity
				.ok(mapper.toCollectionModelDTO(lessonService.findAllByModuleId(moduleId, pageable)));	
		
	}
	
	
	@GetMapping("{lessonId}")
	public ResponseEntity<?> findById(
			@PathVariable UUID moduleId,
			@PathVariable UUID lessonId){
		
		moduleService.findById(moduleId);		
		
		return ResponseEntity
				.ok(mapper.toModelDTO(lessonService.findByLessonIdAndModuleId(moduleId, lessonId)));	
		
	}
	
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public void save(@PathVariable UUID moduleId,  @RequestBody LessonRequestDTO dto) {	
		
		ModuleEntity module = moduleService.findById(moduleId);
		LessonEntity lesson = mapper.toEntity(dto);
		
		lesson.setModule(module);		
		
		lessonService.save(lesson);				
	}
	
	@PutMapping("{lessonId}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void update(
			@PathVariable UUID lessonId,
			@PathVariable UUID moduleId,
			@RequestBody ModuleRequestDTO dto) {
		
		ModuleEntity module = moduleService.findById(moduleId);
		LessonEntity lesson = lessonService.findById(lessonId);		
		
		BeanUtils.copyProperties(
				dto,
				lesson,
				"lessonId"				
		);	
		
		lesson.setModule(module);
		lessonService.save(lesson);	
		
	}
	
	@DeleteMapping("{lessonId}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void delete(
			@PathVariable UUID lessonId,
			@PathVariable UUID moduleId) {		
		
		moduleService.findById(moduleId);
		lessonService.delete(lessonId);
		
	}

}
