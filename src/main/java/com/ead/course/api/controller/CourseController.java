package com.ead.course.api.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.ead.course.api.dto.CourseRequestDTO;
import com.ead.course.api.dto.CourseResponseDTO;
import com.ead.course.api.dto.CourseUserRequestDTO;
import com.ead.course.api.mapper.CourseMapperDTO;
import com.ead.course.model.CourseEntity;
import com.ead.course.service.CourserService;

@RestController
@RequestMapping("v1/courses")
public class CourseController {
	
	private Logger logger =  LogManager.getLogger(this.getClass());
	
	@Autowired
	private CourserService courseService;
	
	@Autowired
	private CourseMapperDTO mapper;	
	
	@GetMapping
	public Page<CourseResponseDTO> findAll(
			@PageableDefault(page = 0, size = 10) Pageable pageable,
			@RequestParam(required = false) UUID userId){			
		
		List<CourseResponseDTO> courseDTO = new ArrayList<>();
		
		if(userId != null) {
			
			courseDTO = mapper.toCollectionModelDTO(courseService.findAllCoursesByUserId(userId,pageable));
			return new PageImpl<>(courseDTO,pageable, courseDTO.size());				
			
		}
		
		courseDTO = mapper.toCollectionModelDTO(courseService.findAll(pageable));		
		return new PageImpl<>(courseDTO,pageable, courseDTO.size());		
	
	}
	
	@GetMapping("{id}")
	public ResponseEntity<?> findById(@PathVariable UUID id){		
		
		 return ResponseEntity
				 .ok(mapper.toDTO(courseService.findById(id)));
	
	}
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public void save(@RequestBody CourseRequestDTO dto) {		
		courseService.save(mapper.toEntity(dto));		
		
	}
	
	@PutMapping("{courseId}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void update(@PathVariable UUID courseId, @RequestBody CourseRequestDTO dto) {		
		CourseEntity course = courseService.findById(courseId);
		BeanUtils.copyProperties(
				dto,
				course,
				"courseId",
				"creationDate",
				"lastUpdateDate"
		);	
		courseService.save(course);			
	} 	
	
	@DeleteMapping("{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void delete(@PathVariable UUID id) {
		
		logger.info("Try to delete course: {}", id);
		
		courseService.delete(id);
		
		logger.info("Delete course success, courseId: {}", id);
	}
	
	@PostMapping("{courseId}/users/subscription")
	@ResponseStatus(HttpStatus.OK)
	public void subscriptionUserInCourse(
			@PathVariable UUID courseId,
			@RequestBody CourseUserRequestDTO dto) {
		
		logger.info("Try to subscription user: {} in course: {}", courseId, dto.getUserId());
		
		courseService.subscriptionUserInCourseAndPublishe(courseId, dto.getUserId());
		
		logger.info("subscription user: {} in course: {} finished", courseId, dto.getUserId());		
		
	}

}
