package com.ead.course.config.datasource;

import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

@Configuration
public class AuthuserDatasourceConfig {				
	
	final String user = System.getenv("DB_USER");
	final String password = System.getenv("DB_PASSWORD");	
	final String host = System.getenv("DB_HOST");	
	final String port = System.getenv("DB_PORT");    
	final String database = System.getenv("DB_NAME");     
	final String schema = System.getenv("DB_SCHEMA");
	
	final String url = "jdbc:postgresql://" + host +  ":" + port + "/" + database + "?currentSchema=" + schema +
			"&serverTimezone=UTC&useUnicode=true&characterEncoding=utf-8";
	
	@Bean
	public DataSource dataSource() {		
		final HikariConfig hikariConfig = new HikariConfig();
		hikariConfig.setDriverClassName("org.postgresql.Driver");
        hikariConfig.setJdbcUrl(url);
        hikariConfig.setUsername(user);
        hikariConfig.setPassword(password);
        hikariConfig.setIdleTimeout(30000L);
        hikariConfig.setMaximumPoolSize(1);
        hikariConfig.setLeakDetectionThreshold(120000L);
        hikariConfig.setPoolName("course_pool");
        hikariConfig.setConnectionTestQuery("SELECT 1");        
        return new HikariDataSource(hikariConfig);		
	}
	
	@Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource dataSource) {
        final HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        
        final Properties jpaProperties = new Properties();
        jpaProperties.setProperty("hibernate.hbm2ddl.auto", "none");
        jpaProperties.setProperty("hibernate.jdbc.lob.non_contextual_creation", "true");
        jpaProperties.setProperty("hibernate.dialect", "org.hibernate.dialect.PostgreSQLDialect");       
        jpaProperties.setProperty("show_sql", "true");
        jpaProperties.setProperty("format_sql", "true");
        jpaProperties.setProperty("use_sql_comments", "true");
        jpaProperties.setProperty("open-in-view", "false");              
        
        final LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
        factory.setJpaVendorAdapter(vendorAdapter);
        factory.setPackagesToScan("com.ead.course");
        factory.setDataSource(dataSource);
        factory.setJpaProperties(jpaProperties);
        return factory;
    }
	
	@Bean
	public PlatformTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) {
	    final JpaTransactionManager txManager = new JpaTransactionManager();
	    txManager.setEntityManagerFactory(entityManagerFactory);
	    return txManager;
	}

}
