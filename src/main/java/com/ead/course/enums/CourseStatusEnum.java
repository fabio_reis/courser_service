package com.ead.course.enums;

import com.ead.course.exceptions.EntityRegistredException;

public enum CourseStatusEnum {
	
	INPROGRESS("INPROGRESS"),
	CONCLUDED("CONCLUDED");	
	
	private String description;
	
	CourseStatusEnum(String description) {		
		this.description = description;
	}	
	
	public static CourseStatusEnum parse(final String description) {		
		for(CourseStatusEnum enumerator : CourseStatusEnum.values()) {
			if(enumerator.description.equals(description))
				return enumerator;
		}
		throw new EntityRegistredException("status invalid");
	}

}
