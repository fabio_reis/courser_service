package com.ead.course.enums;

import com.ead.course.exceptions.EntityRegistredException;

public enum CourseLevelEnum {
	
	BEGINNER("BEGINNER"),
	INTERMEDIARY("INTERMEDIARY"),
	ADVANCED("ADVANCED");
	
	private String description;
	
	CourseLevelEnum(String description) {		
		this.description = description;
	}	
	
	public static CourseLevelEnum parse(final String description) {		
		for(CourseLevelEnum enumerator : CourseLevelEnum.values()) {
			if(enumerator.description.equalsIgnoreCase(description))
				return enumerator;
		}
		throw new EntityRegistredException("courseLevel invalid");
	}

}
