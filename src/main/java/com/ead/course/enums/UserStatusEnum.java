package com.ead.course.enums;

import com.ead.course.exceptions.EntityRegistredException;

public enum UserStatusEnum {
	
	ACTIVE("ACTIVE"),
	BLOCKED("BLOCKED");
	
	private String description;
	
	UserStatusEnum(String description) {		
		this.description = description;
	}	
	
	public String getDescription() {
		return this.description;
	}
	
	public static UserStatusEnum parse(final String description) {		
		for(UserStatusEnum enumerator : UserStatusEnum.values()) {
			if(enumerator.description.equalsIgnoreCase(description))
				return enumerator;
		}
		throw new EntityRegistredException("userStatus invalid");
	}

}
