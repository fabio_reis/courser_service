package com.ead.course.enums;

import com.ead.course.exceptions.ValueNotFoundException;

public enum UserTypeEnum {
	
	ADMIN("ADMIN"),
	STUDENT("STUDENT"),
	INSTRUCTOR("INSTRUCTOR");
	
	private String description;
	
	UserTypeEnum(String descripion) {
		this.description = descripion;
	}
	
	public static UserTypeEnum parse(String description) {		
		for(UserTypeEnum type : UserTypeEnum.values()) {
			if(type.description.equalsIgnoreCase(description)) 
				return type;			
		}		
		throw new ValueNotFoundException("UserType does not exists");
	} 
	

}
