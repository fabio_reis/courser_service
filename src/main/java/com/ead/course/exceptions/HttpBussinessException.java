package com.ead.course.exceptions;

public class HttpBussinessException extends BusinessException {
	
	public HttpBussinessException(String message) {
		super(message);		
	}

	private static final long serialVersionUID = 7921977654546993993L;

}
