package com.ead.course.exceptions;

public class EntityRegistredException extends BusinessException {

	private static final long serialVersionUID = 1L;
	
	public EntityRegistredException(String message) {
		super(message);
	}

}
