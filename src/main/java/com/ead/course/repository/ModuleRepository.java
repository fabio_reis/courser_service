package com.ead.course.repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ead.course.model.CourseEntity;
import com.ead.course.model.ModuleEntity;

@Repository
public interface ModuleRepository extends JpaRepository<ModuleEntity, UUID> {
	
	List<ModuleEntity> findByCourse(CourseEntity course);		
		
	@Query("SELECT m FROM ModuleEntity m WHERE m.course.courseId = :courseId")
	Page<ModuleEntity> findAllByCourseId(UUID courseId, Pageable pageable);
	
	@Query("SELECT m FROM ModuleEntity m WHERE m.course.courseId = :courseId AND m.moduleId = :moduleId")
	Optional<ModuleEntity> findByModuleIdAndCourseId(UUID moduleId, UUID courseId);

}
