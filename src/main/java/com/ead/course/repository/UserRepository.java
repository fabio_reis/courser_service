package com.ead.course.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ead.course.model.UserEntity;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, UUID> {	
	
	@Query("FROM UserEntity c JOIN FETCH c.courses c WHERE c.courseId = :courseId ")
	List<UserEntity> findAllByUserByCourseId(UUID courseId, Pageable pageable);
	
	@Query(value = "SELECT EXISTS (SELECT true FROM tb_courses_users u WHERE u.user_id = :userId AND u.course_id = :courseId)", nativeQuery = true)
	boolean existsUserInCourse(UUID userId, UUID courseId);	
	
	@Modifying
	@Query(value="DELETE FROM TB_COURSES_USERS c WHERE c.user_id = :userId", nativeQuery=true)
	void deleteCoursesUsers(UUID userId);
	
	@Modifying
	@Query(value="DELETE FROM TB_USERS u WHERE u.user_id = :userId", nativeQuery=true)
	void delete(UUID userId);

}
