package com.ead.course.repository;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ead.course.model.CourseEntity;

@Repository
public interface CourseRepository extends JpaRepository<CourseEntity, UUID> {
	
	String selectAllCoursesByUserId = "" + 
		"SELECT " + 
			"c.* " +
		"FROM TB_COURSES c " +
		"JOIN TB_COURSES_USERS cu ON c.course_id = cu.course_id " +
		"JOIN TB_USERS u ON u.user_id = cu.user_id " +
		"WHERE u.user_id = :userId";
	
	String CountAllCoursesByUserId = "" + 
			"SELECT " + 
				"COUNT(c.course_id)" +
			"FROM TB_COURSES c " +
			"JOIN TB_COURSES_USERS cu ON c.course_id = cu.course_id " +
			"JOIN TB_USERS u ON u.user_id = cu.user_id " +
			"WHERE u.user_id = :userId";	
	
	@Query(value="SELECT c.* FROM course c WHERE LOWER(c.name) = LOWER(:name) LIMIT 1", nativeQuery = true)
	Optional<CourseEntity> findByName(String name);
	
	@Query(value = "SELECT EXISTS(SELECT true FROM TB_COURSES_USERS c WHERE c.course_id = :courseId AND c.user_id = :userId)", nativeQuery = true)
	boolean existsUserInCourse(UUID courseId, UUID userId);
		
	@Modifying
	@Query(value = "INSERT INTO TB_COURSES_USERS(course_id, user_id) VALUES(:courseId, :userId)", nativeQuery = true)
	void subscriptionUserInCourse(UUID courseId, UUID userId);
	
	@Modifying
	@Query(value="DELETE FROM TB_COURSES_USERS c WHERE c.course_id = :courseId", nativeQuery=true)
	void deleteCoursesUsers(UUID courseId);
	
	@Query(value=selectAllCoursesByUserId, countQuery=CountAllCoursesByUserId, nativeQuery=true)
	Page<CourseEntity> findAllCoursesByUserId(@Param("userId") UUID userId, Pageable pageable);

}
