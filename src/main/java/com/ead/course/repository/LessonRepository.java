package com.ead.course.repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ead.course.model.LessonEntity;
import com.ead.course.model.ModuleEntity;

@Repository
public interface LessonRepository extends JpaRepository<LessonEntity, UUID> {
	
	List<LessonEntity> findByModule(ModuleEntity module);
	
	@Modifying
	@Query(value="DELETE FROM TB_LESSONS l WHERE l.module_id IN(:moduleIds) ", nativeQuery=true)
	public void deleteByModuleIdInBatch(@Param("moduleIds") List<UUID> moduleIds);
	
	@Query("SELECT l FROM LessonEntity l WHERE l.module.moduleId = :moduleId")
	List<LessonEntity> findAllByModuleId(UUID moduleId, Pageable pageable);
	
	@Query("SELECT l FROM LessonEntity l WHERE l.module.moduleId = :moduleId AND l.lessonId = :lessonId")
	Optional<LessonEntity> findAllByLessonIdAndModuleId(UUID moduleId, UUID lessonId);

}
