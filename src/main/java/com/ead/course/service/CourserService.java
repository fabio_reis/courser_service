package com.ead.course.service;

import static com.ead.course.utils.ValidationUtils.isNull;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.ead.course.broker.dto.UserNotificationDTO;
import com.ead.course.broker.publisher.UserNotificationPublisher;
import com.ead.course.exceptions.EntityNotFoundException;
import com.ead.course.exceptions.EntityRegistredException;
import com.ead.course.model.CourseEntity;
import com.ead.course.model.ModuleEntity;
import com.ead.course.model.UserEntity;
import com.ead.course.repository.CourseRepository;

@Service
public class CourserService {
	
	@Autowired
	private CourseRepository courseRepository;	
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private ModuleService moduleService;
	
	@Autowired
	private LessonService lessonService;
	
	@Autowired
	private UserNotificationPublisher userNotificationPublisher;
	
	@Transactional
	public void save(CourseEntity course) {		
		
		validationCourse(course);			
		CourseEntity courseDB = findByName(course.getName());
		
		if(!isNull(courseDB)) throw new EntityRegistredException("course name already exists");			
		
		courseRepository.save(course);		
	}	
	
	public CourseEntity findByName(String courseName) {
		return courseRepository.findByName(courseName)
				.orElse(null);				
	}
	
	public List<CourseEntity> findAll(Pageable pageable) {			
		return courseRepository.findAll(pageable)	
			.getContent();			
	}		
	
	public List<CourseEntity> findAllCoursesByUserId(UUID userId, Pageable pageable) {		
		return courseRepository.findAllCoursesByUserId(userId, pageable)
				.getContent();
	}	
	
	public CourseEntity findById(UUID courseId) {		
		return courseRepository.findById(courseId)
				.orElseThrow(
					() -> new EntityNotFoundException(String.format("Course not found: %s", courseId)));		
		
	}	
	
	@Transactional
	public void delete(UUID courseId) {
		
		var course = findById(courseId);
		
		List<ModuleEntity> modules = moduleService.findByCourse(course);			
		
		if(!modules.isEmpty()) {
			
			List<UUID> modulesId = modules.stream().map(module -> module.getModuleId())
					.collect(Collectors.toList());
			
			lessonService.deleteByModuleIdInBatch(modulesId);
			
			moduleService.deleteAllInBatch(modulesId);			
			
		}	
		
		courseRepository.deleteCoursesUsers(courseId);
		courseRepository.delete(course);		
		
	}		
	
	@Transactional
	public void subscriptionUserInCourse(UUID courseId, UUID userId) {
		if(courseRepository.existsUserInCourse(courseId,userId))
			throw new EntityRegistredException("User already registred in course");
		
		CourseEntity course = findById(courseId);
		UserEntity user = userService.findById(userId);
		
		courseRepository
			.subscriptionUserInCourse(course.getCourseId(), user.getUserId());
	
	}
	
	@Transactional
	public void subscriptionUserInCourseAndPublishe(UUID courseId, UUID userId) {
		
		 subscriptionUserInCourse(courseId, userId);
		 
		 userNotificationPublisher.pupishNotification(
				 new UserNotificationDTO(courseId, userId)
		);
	} 
	
	private void validationCourse(CourseEntity course) {
		
		if(isNull(course.getName()))
			throw new EntityRegistredException("name empty");
		
		if(isNull(course.getDescription()))
			throw new EntityRegistredException("description empty");
		
		if(isNull(course.getCourseStatus()))
			throw new EntityRegistredException("courseStatus empty");
		
		if(isNull(course.getCourseLevel()))
			throw new EntityRegistredException("courseLevel empty");
		
		if(isNull(course.getUserInstructor()))
			throw new EntityRegistredException("userInstructor empty");
		
		UserEntity user = userService.findById(course.getUserInstructor());
		
		if(!user.isActive())
			throw new EntityRegistredException("User disabled");
		
		if(user.isStudent())
			throw new EntityRegistredException("Student can not register courses");
	}

}
