package com.ead.course.service;

import static com.ead.course.utils.ValidationUtils.isNull;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.ead.course.exceptions.EntityNotFoundException;
import com.ead.course.exceptions.EntityRegistredException;
import com.ead.course.model.CourseEntity;
import com.ead.course.model.ModuleEntity;
import com.ead.course.repository.ModuleRepository;

@Service
public class ModuleService {
	
	@Autowired
	private ModuleRepository moduleRepository;
	
	@Autowired
	private LessonService lessonService;	
	
	@Autowired	
	private CourserService courseService;
	
	public List<ModuleEntity> findByCourse(CourseEntity course){		
		return moduleRepository.findByCourse(course);		
	}
	
	public List<ModuleEntity> findAllByCourseId(UUID courseId, Pageable pageable){
		
		var course = courseService.findById(courseId);
		return moduleRepository.findAllByCourseId(course.getCourseId(), pageable).getContent();
	}
	
	public ModuleEntity findByModuleIdAndCourseId(UUID moduleId, UUID courseId) {		
		
		return moduleRepository.findByModuleIdAndCourseId(moduleId, courseId)
			.orElseThrow(() -> new EntityNotFoundException(String.format("Module not found: %s", moduleId)));		
				
	}
	
	public ModuleEntity findById(UUID moduleId) {		
		
		return moduleRepository.findById(moduleId)
			.orElseThrow(() -> new EntityNotFoundException(String.format("Module not found: %s", moduleId)));		
				
	}
	
	@Transactional
	public void deleteAllInBatch(List<UUID> moduleIds) {
		moduleRepository.deleteAllByIdInBatch(moduleIds);
	}
	
	@Transactional
	public void delete(UUID moduleId) {
		
		ModuleEntity module = findById(moduleId);
		
		lessonService.deleteByModuleIdInBatch(Arrays.asList(module.getModuleId()));
		
		moduleRepository.delete(module);	
		
	}
	
	@Transactional
	public void save(ModuleEntity module) {
		
		if(isNull(module.getTitle()))
			throw new EntityRegistredException("title empty");
		
		if(isNull(module.getDescription()))
			throw new EntityRegistredException("description empty");
		
		
		moduleRepository.save(module);
	}

}
