package com.ead.course.service;

import static com.ead.course.utils.ValidationUtils.isNull;

import java.util.List;
import java.util.UUID;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.ead.course.exceptions.EntityNotFoundException;
import com.ead.course.exceptions.EntityRegistredException;
import com.ead.course.model.LessonEntity;
import com.ead.course.repository.LessonRepository;

@Service
public class LessonService {
	
	@Autowired
	private LessonRepository lessonRepository;	
	
	public List<LessonEntity> findAllByModuleId(UUID moduleId, Pageable pageable){
		return lessonRepository.findAllByModuleId(moduleId, pageable);
	}
	
	public LessonEntity findByLessonIdAndModuleId(UUID moduleId, UUID lessonId){
		return lessonRepository.findAllByLessonIdAndModuleId(moduleId, lessonId)
				.orElseThrow(() -> new EntityNotFoundException(String.format("Lesson not found: %s", lessonId)));
	}
	
	public LessonEntity findById(UUID lessonId) {
		return lessonRepository.findById(lessonId)
				.orElseThrow(() -> new EntityNotFoundException(String.format("Lesson not found: %s", lessonId)));
	}
	
	@Transactional
	public void save(LessonEntity lesson) {
		
		if(isNull(lesson.getTitle()))
			throw new EntityRegistredException("title empty");
		
		if(isNull(lesson.getVideoUrl()))
			throw new EntityRegistredException("videoUrl empty");
		
		lessonRepository.save(lesson);
	}
	
	@Transactional
	public void deleteByModuleIdInBatch(List<UUID> moduleIds) {
		lessonRepository.deleteByModuleIdInBatch(moduleIds);		
	}
	
	@Transactional
	public void delete(UUID lessonId) {
		
		LessonEntity lesson = findById(lessonId);
		
		lessonRepository.delete(lesson);
	}

}
