package com.ead.course.service;

import static com.ead.course.utils.ValidationUtils.isNull;

import java.util.List;
import java.util.UUID;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.ead.course.exceptions.EntityNotFoundException;
import com.ead.course.exceptions.EntityRegistredException;
import com.ead.course.model.UserEntity;
import com.ead.course.repository.UserRepository;


@Service
public class UserService {
	
	@Autowired
	private UserRepository userRepository;	
	
	public List<UserEntity> findAllUsersByCourseId(UUID courseId, Pageable pageable){
		return userRepository.findAllByUserByCourseId(courseId, pageable);		
	}
	
	public UserEntity findById(UUID userId) {
		return userRepository
				.findById(userId)
				.orElseThrow(() -> new EntityNotFoundException(String.format("User not found: %s", userId)));
	}
	
	
	public void checkExistsUserInCourse(UUID userId, UUID courseId) {		
		if(userRepository.existsUserInCourse(userId, courseId))
			throw new EntityRegistredException("User already subscription in course"); 
	}	
	
	@Transactional
	public void save(UserEntity user) {
		validateUser(user);
		userRepository.save(user);
	}
	
	
	public void update(UserEntity user) {			
		var userDb = userRepository.getById(user.getUserId());
		if(userDb != null) save(user);
	}
	
	@Transactional
	public void delete(UserEntity user) {
		var userDb = userRepository.getById(user.getUserId());		
	  	if(userDb != null) {
	  		userRepository.deleteCoursesUsers(user.getUserId());
	  		userRepository.delete(user.getUserId());
	  	} 
	  	
	}
	
	private void validateUser(UserEntity user) {
		if(isNull(user.getUserId()))
			throw new EntityRegistredException("userId is empty");
		
		if(isNull(user.getFullName()))
			throw new EntityRegistredException("fullName is empty");
		
		if(isNull(user.getUserStatus()))
			throw new EntityRegistredException("userStatus is empty");
		
		if(isNull(user.getUserType()))
			throw new EntityRegistredException("userType is empty");
	}
	
}
