package com.ead.course.model;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.ead.course.enums.UserStatusEnum;
import com.ead.course.enums.UserTypeEnum;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "TB_USERS")
@Getter @Setter @EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class UserEntity {
	
	@Id	@Column(name = "user_id")
	@EqualsAndHashCode.Include
	private UUID userId;	
	
	private String email;
	private String cpf;
	
	@Column(name = "full_name")
	private String fullName;
	
	@Column(name = "user_status")
	private String userStatus;
	
	@Column(name = "user_type")
	private String userType;
	
	@Column(name = "image_url")
	private String imageUrl;	
	
	@ManyToMany(mappedBy = "users")
	private List<CourseEntity> courses = new ArrayList<>();
	
	public boolean isActive() {
		return UserStatusEnum.ACTIVE.name().equalsIgnoreCase(this.userStatus);		
	}
	
	public boolean isStudent() {
		return UserTypeEnum.STUDENT.name().equalsIgnoreCase(this.userType);		
	}

}
