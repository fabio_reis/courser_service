package com.ead.course.model;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "TB_LESSONS")
@Getter @Setter @EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class LessonEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@EqualsAndHashCode.Include
	@Column(name = "lesson_id")
	private UUID lessonId;
	
	@Column(name = "video_url", nullable = false)
	private String videoUrl;
	
	@Column(nullable = false)
	private String title;	
	
	private String description;	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "module_id", nullable = false)
	private ModuleEntity module;

}
