package com.ead.course.model;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.ead.course.enums.CourseLevelEnum;
import com.ead.course.enums.CourseStatusEnum;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "TB_COURSES")
@Getter @Setter @EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class CourseEntity {
	
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	@EqualsAndHashCode.Include
	@Column(name = "course_id")
	private UUID courseId;
	
	private String name;
	private String description;
	
	@Column(name = "image_url")
	private String imageUrl;
	
	@Column(name = "user_instructor")
	private UUID userInstructor;
	
	@CreationTimestamp
	@Column(name="creation_date", nullable = false)
	private OffsetDateTime creationDate;
	
	@UpdateTimestamp
	@Column(name="last_update_date", nullable = false)
	private OffsetDateTime lastUpdateDate;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "course_status", nullable = false)
	private CourseStatusEnum courseStatus;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "course_level", nullable = false)	
	private CourseLevelEnum courseLevel;	
	
	@ManyToMany
	@JoinTable(
		name = "TB_COURSES_USERS",
		joinColumns = @JoinColumn(name="course_id"),
		inverseJoinColumns = @JoinColumn(name="user_id")		
	)
	private List<UserEntity> users = new ArrayList<>();	

}
