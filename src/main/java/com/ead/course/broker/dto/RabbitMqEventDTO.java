package com.ead.course.broker.dto;

import java.io.Serializable;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.JsonNode;

import lombok.NoArgsConstructor;
import lombok.ToString;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
@ToString
public class RabbitMqEventDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	public RabbitMqEventDTO(JsonNode payload, String action, String routingKey) {
		this.payload = payload;
		this.action = action;
		this.routingKey = routingKey;
	}

	public RabbitMqEventDTO(JsonNode payload, String action) {
		this.payload = payload;
		this.action = action;
	}

	@JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
	public String eventDate;	
	public JsonNode payload;
	public String action;
	public String producer = "authuser_service";
	public String routingKey;
	public String id = UUID.randomUUID().toString();
	
}
