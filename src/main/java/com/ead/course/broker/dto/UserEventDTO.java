package com.ead.course.broker.dto;

import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@ToString
public class UserEventDTO {	
	
	private UUID userId;		
	private String email;	
	private String fullName;
	private String userStatus;
	private String userType;
	private String phoneNumber;	
	private String cpf;		
	private String imageUrl;	

}
