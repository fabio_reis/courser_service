package com.ead.course.broker.dto;

import java.util.UUID;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter
@NoArgsConstructor
@ToString
public class UserNotificationDTO {
	
	public UserNotificationDTO(UUID courseId, UUID userId) {
		this.courseId = courseId;
		this.userId = userId;
	}
	
	private UUID courseId;
	private UUID userId;
	private String message;
	private String title;

}
