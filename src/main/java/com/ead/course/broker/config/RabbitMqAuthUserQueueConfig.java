package com.ead.course.broker.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import org.springframework.amqp.core.Queue;

@Configuration
public class RabbitMqAuthUserQueueConfig {
	
	@Value("${decoder.broker.exchange.fanoutDecoder}")
	private String decoderExchange;
	
	@Value("${decoder.broker.queue.authuserqueue}")
	private String authUserQueue;
	
	@Bean
	public FanoutExchange decoderExchange() {
		return new FanoutExchange(decoderExchange);
	}
	
	@Bean
	public Queue authuserQueue() {
		return new org.springframework.amqp.core.Queue(authUserQueue);
	}
	
	@Bean
	public Binding bindQueue() {
		return BindingBuilder
			.bind(authuserQueue())			
			.to(decoderExchange());		
	}

}
