package com.ead.course.broker.config;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageBuilder;
import org.springframework.amqp.core.MessageDeliveryMode;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ead.course.exceptions.ParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class RabbitSimpleMessage {
	
	private Logger logger = LogManager.getLogger(this.getClass());
	
	@Autowired
	private ObjectMapper mapper;
	
	public Message toJson(Object payload) {	
		
		try {
			
			String json = mapper.writeValueAsString(payload);
			
			return MessageBuilder
					.withBody(json.getBytes())
					.setContentType(MessageProperties.CONTENT_TYPE_TEXT_PLAIN)
					.setContentEncoding("UTF-8")
					.setDeliveryMode(MessageDeliveryMode.PERSISTENT)
					//.setHeader("__typeId__", "com.ead.apiproducer.etc")
					.build();			
		
		} catch (JsonProcessingException e) {			
			logger.info("Error try to converte message: {}", payload);
			throw new ParseException("Error try to converte message: " + payload.toString());
		}		
						
	}

}
