package com.ead.course.broker.publisher;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.ead.course.broker.config.RabbitSimpleMessage;
import com.ead.course.broker.dto.UserNotificationDTO;

@Component
public class UserNotificationPublisher {
	
	private Logger logger =  LogManager.getLogger(this.getClass());
	
	@Value("${decoder.broker.exchange.notificationCommandExchange}")
	private String USER_NOTIFIER_EXCHANGE;
	
	@Value("${decoder.broker.key.notificationCommandKey}")
	private String USER_NOTIFIER_KEY;	
	
	@Autowired
	private RabbitTemplate template;
	
	@Autowired
	private RabbitSimpleMessage simpleMessage;

	
	public void pupishNotification(UserNotificationDTO payload) {
		
		payload.setTitle("Welcome to the course");
		payload.setMessage("user: " + payload.getUserId() + " was registred in course: " + payload.getCourseId());
		
		template.send(
			USER_NOTIFIER_EXCHANGE,
			USER_NOTIFIER_KEY,
			simpleMessage.toJson(payload)		
		);
		
		logger.info("User notification send message: {}", payload.toString());		
	}

}
