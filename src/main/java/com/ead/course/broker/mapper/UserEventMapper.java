package com.ead.course.broker.mapper;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ead.course.broker.dto.UserEventDTO;
import com.ead.course.exceptions.ParseException;
import com.ead.course.model.UserEntity;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class UserEventMapper {
	
	Logger logger =  LogManager.getLogger(this.getClass());
	
	@Autowired
	private ObjectMapper objectMapper;
	
	@Autowired
	private ModelMapper modelMapper;
	
	public UserEntity toEntity(JsonNode payload) {
		try {
			return modelMapper.map(objectMapper.readValue(payload.toString(), UserEventDTO.class), UserEntity.class);			
		} catch (JsonProcessingException e) {
			logger.error("Error try to parse payload");
			throw new ParseException("Error try to parse payload");
		}
	}

}
