package com.ead.course.broker.consumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ead.course.broker.dto.RabbitMqEventDTO;
import com.ead.course.broker.mapper.UserEventMapper;
import com.ead.course.service.UserService;

@Component
public class UserEventConsumer {	
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private UserEventMapper mapper;
	
	public void consume(RabbitMqEventDTO event) {			
		
		switch(event.action) {
			case "UPDATE": userService.update(mapper.toEntity(event.payload));;
				break;
			case "CREATE": userService.save(mapper.toEntity(event.payload));
				break;
			case "DELETE": userService.delete(mapper.toEntity(event.payload));
				break;
			default:
				break;
		}	
	}
}
