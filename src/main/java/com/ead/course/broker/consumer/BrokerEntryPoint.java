package com.ead.course.broker.consumer;

import static com.ead.course.utils.ValidationUtils.isNull;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import com.ead.course.broker.dto.RabbitMqEventDTO;
import com.ead.course.exceptions.PayloadNullException;

@Component
@RabbitListener(queues = {"${decoder.broker.queue.authuserqueue}" })
public class BrokerEntryPoint {
	
	Logger logger =  LogManager.getLogger(this.getClass());

	private final String AUTHUSER_PRODUCER = "authuser_service";

	@Autowired
	private UserEventConsumer userEventConsumer;

	@RabbitHandler
	public void receive(@Payload RabbitMqEventDTO event) {		
		
		if(isNull(event) || isNull(event.payload))
			throw new PayloadNullException("Event payload is null");		
		
		 switch (event.producer) {
		 	case AUTHUSER_PRODUCER: userEventConsumer.consume(event);
		 		break;
		 	default:
		 		break;		 		
	 	}		
		 
		 logger.info("UserEvent: " + event.action + "consumed: {}", event.toString());
	}

}
