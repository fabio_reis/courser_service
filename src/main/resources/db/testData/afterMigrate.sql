TRUNCATE TABLE TB_COURSES CASCADE;
TRUNCATE TABLE TB_LESSONS CASCADE;
TRUNCATE TABLE TB_MODULES CASCADE;
TRUNCATE TABLE TB_USERS CASCADE;
TRUNCATE TABLE TB_COURSES_USERS CASCADE;

INSERT INTO TB_USERS(
	user_id,
	cpf,	
	email,
	full_name,
	image_url,	
	user_type	
)
VALUES(
	(SELECT user_id FROM auth_user.tb_users ORDER BY user_name ASC LIMIT 1),
	'05561373536',	
	'fabio_dos_reis@outlook.com',
	'Fábio Reis',
	'image',	
	'ADMIN'	
);

INSERT INTO TB_USERS(
	user_id,
	cpf,	
	email,
	full_name,
	image_url,	
	user_type	
)
VALUES(
	(SELECT user_id FROM auth_user.tb_users ORDER BY user_name DESC LIMIT 1),
	'13905589089',	
	'carol_cunha@outlook.com',
	'Caroline Cunha',
	'image',
	'STUDENT'	
);

INSERT INTO TB_COURSES(
	course_id,
	name,
	description,
	image_url,
	user_instructor,
	creation_date,
	last_update_date,
	course_status,
	course_level	
)VALUES(
	(SELECT md5(random()::text || clock_timestamp()::text)::uuid),
	'Java',
	'Complete course Java development',
	'image url',
	(SELECT md5(random()::text || clock_timestamp()::text)::uuid),
	timezone('utc', now()),
	timezone('utc', now()),
	'INPROGRESS',
	'BEGINNER'
);


INSERT INTO TB_COURSES(
	course_id,
	name,
	description,
	image_url,
	user_instructor,
	creation_date,
	last_update_date,
	course_status,
	course_level	
)VALUES(
	(SELECT md5(random()::text || clock_timestamp()::text)::uuid),
	'SQL',
	'Course MySQL',
	'image url',
	(SELECT md5(random()::text || clock_timestamp()::text)::uuid),
	timezone('utc', now()),
	timezone('utc', now()),
	'CONCLUDED',
	'INTERMEDIARY'
);


INSERT INTO TB_MODULES (
	module_id,
	title,
	description,	
	course_id			
)VALUES(
	(SELECT md5(random()::text || clock_timestamp()::text)::uuid),
	'JAVA BASIC',
	'BASIC',	
	(SELECT course_id FROM TB_COURSES ORDER BY CREATION_DATE ASC LIMIT 1)	
);

INSERT INTO TB_MODULES (
	module_id,
	title,
	description,	
	course_id			
)VALUES(
	(SELECT md5(random()::text || clock_timestamp()::text)::uuid),
	'JAVA ADVANCED',
	'ADVANCED',	
	(SELECT course_id FROM TB_COURSES ORDER BY CREATION_DATE DESC LIMIT 1)	
);


INSERT INTO TB_LESSONS (
	lesson_id,
	video_url,
	title,
	description,	
	module_id	
)VALUES(
	(SELECT md5(random()::text || clock_timestamp()::text)::uuid),
	'video url',
	'POO',
	'POO base definition',	
	(SELECT module_id FROM course.TB_MODULES ORDER BY module_id ASC LIMIT 1)	
);


INSERT INTO TB_LESSONS (
	lesson_id,
	video_url,
	title,
	description,	
	module_id	
)VALUES(
	(SELECT md5(random()::text || clock_timestamp()::text)::uuid),
	'video url2',
	'Constructors',
	'How works the constructors',	
	(SELECT module_id FROM course.TB_MODULES ORDER BY module_id DESC LIMIT 1)	
);


INSERT INTO TB_COURSES_USERS(
	course_id,
	user_id	
)VALUES(
	(SELECT course_id FROM TB_COURSES ORDER BY course_id DESC LIMIT 1),
	(SELECT user_id FROM auth_user.tb_users ORDER BY user_name DESC LIMIT 1)	
);


INSERT INTO TB_COURSES_USERS(
	course_id,
	user_id	
)VALUES(
	(SELECT course_id FROM TB_COURSES ORDER BY course_id ASC LIMIT 1),
	(SELECT user_id FROM auth_user.tb_users ORDER BY user_id ASC LIMIT 1)
);
