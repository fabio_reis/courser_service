CREATE TABLE TB_COURSES (
    course_id uuid not null,
    name varchar(100) not null,
    description varchar(100) not null,
    image_url varchar(100),
    user_instructor uuid,
    creation_date timestamp not null,
    last_update_date timestamp not null,
    course_status varchar(20) not null,
    course_level varchar(20) not null,
    primary key(course_id)    	
)