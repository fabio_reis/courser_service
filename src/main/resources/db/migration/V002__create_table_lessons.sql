CREATE TABLE TB_LESSONS (
    lesson_id uuid not null,
    video_url varchar(100) not null,
    title varchar(100) not null,
    description varchar(100),        
    primary key(lesson_id)    	
)