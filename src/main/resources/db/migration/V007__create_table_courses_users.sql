CREATE TABLE TB_COURSES_USERS (    
    course_id uuid not null,
    user_id uuid not null,
    FOREIGN KEY(course_id) REFERENCES TB_COURSES(course_id),
    FOREIGN KEY(user_id) REFERENCES TB_USERS(user_id)    
)