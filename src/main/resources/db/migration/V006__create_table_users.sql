create table TB_USERS (
    user_id uuid not null,
    cpf varchar(20),   
    email varchar(50) not null unique,
    full_name varchar(150) not null,
    image_url varchar(30),      
    user_status varchar(30) default 'ACTIVE' not null,
    user_type varchar(30) default 'STUDENT' not null,
    primary key (user_id)
)