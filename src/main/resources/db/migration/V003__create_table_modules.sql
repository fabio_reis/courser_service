CREATE TABLE TB_MODULES (
    module_id uuid not null,
    title varchar(100) not null,   
    description varchar(100) not null,      
    primary key(module_id)    	
)